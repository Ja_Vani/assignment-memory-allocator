#include "mem.h"
#include <stdio.h>


void* map_pages(void const* addr, size_t length, int additional_flags);

void* the_heap;

void test1() {
    int *first = _malloc(sizeof(int));
    int *second = _malloc(sizeof(int));

    *first = -123;
    *second = 3432;

    printf("%d\n%d\n", *first, *second);
    printf("Test 1 passed!\n");

    _free(first);
    _free(second);
}

void test2() {
    int *first = _malloc(sizeof(int));
    int *second = _malloc(sizeof(int));

    *first = -123;
    *second = 3432;

    printf("%d\n%d\n", *first, *second);
    _free(first); 

    printf("Test 2 passed!\n");
    _free(second);
}

void test3() {
    int *first = _malloc(sizeof(int));
    long long *second = _malloc(sizeof(long long));
    int *third = _malloc(sizeof(int));

    *first = -123;
    *second = 31302587432;
    *third = 124124;

    printf("%d\n%lld\n%d\n", *first, *second, *third);
    _free(first);   
    _free(third);  

    printf("Test 3 passed!\n");
    _free(second);
}

void test4() {
    int *first = _malloc(sizeof(int) * 64);

    for (size_t i = 0; i < 64; i++) {
        first[i] = (int)(i * i * i);
        printf("%d ", first[i]);
    }

    printf("\n");
    _free(first);
    printf("Test 4 passed!\n");
}

void test5() {
    int *first = _malloc(sizeof(int) * 8);  
    int *second = _malloc(sizeof(int) * 32); 
    
    for (size_t i = 0; i < 32; i++) {
        second[i] = (int)(i * i * i);
        printf("%d ", second[i]);
    }

    printf("\n");
    _free(first);   

    first = (int*) _malloc(sizeof(int) * 128);  

    for (size_t i = 0; i < 128; i++) {
        first[i] = (int)(i * i);
        printf("%d ", first[i]);
    }

    printf("\n");
    _free(second);
    _free(first);

    printf("Test 5 passed!\n");
}

int main(int argc, char** argv) {
     if (argc > 1) {
        if (((char)argv[1][0] - '0') < 1 || ((char)argv[1][0] - '0') > 5) {
            printf("Invalid argument format.\n   Run all tests: main\n  Run particular test: main [1..5]\n");
        } else {
            heap_init(32);
            switch ((char) (argv[1][0] - '0')) {
                case 1:
                    test1();
                    break;
                case 2:
                    test2();
                    break;
                case 3:
                    test3();
                    break;
                case 4:
                    test4();
                    break;
                case 5:
                    test5();
                    break;
            }
        }
    } else {
        heap_init(32);
        test1();
        test2();
        test3();
        test4();
        test5();
    }
	return 0;
}
